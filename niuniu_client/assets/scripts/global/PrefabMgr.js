cc.Class({
    extends: cc.Component,

    properties: {
        _wait : null,        //当前的等待界面  7
        _alert : null,      //当前的通知界面  6
        _layout : null,      //当前的弹出层界面 5
        
        wait : cc.Prefab,
        alert : cc.Prefab,

        quick : cc.Prefab,
        createRoom : cc.Prefab,
        joinRoom : cc.Prefab,
        setting : cc.Prefab,
        union : cc.Prefab,
        unionCreate : cc.Prefab,
        safeBox : cc.Prefab,
        kefu : cc.Prefab,
        statement : cc.Prefab,

        hold : cc.Prefab,
        _hold : null,
    },

    // use this for initialization
    onLoad: function () {
        console.log("PrefabMgr onLoad...");
        if(!cc.vv){
            return;
        }
        this._wait = null;
        this._alert = null;
        this._layout = null;
        
        cc.vv.prefabMgr = this;
        cc.vv.eventManager = this.node.getComponent("EventManager");
        cc.game.addPersistRootNode(this.node);

        this.callPoker = {};
        this.pokersSelected = null;
        this.push1 = null;
        this.push2 = null;
        this.push3 = null;
        this.push4 = null;
        this.push5 = null;
        this.push6 = null;
        this.push7 = null;
        this.push8 = null;
        this.push9 = null;
        this.push10 = null;
        this.push11 = null;
        this.push12 = null;
        this.push13 = null;
    },
    
    quickOpen : function (parent) {
        this.layoutOpen2(parent,this.quick);
    },
    createRoomOpen : function (parent) {
        this.layoutOpen(parent,this.createRoom);
    },
    joinRoomOpen : function (parent) {
        this.layoutOpen(parent,this.joinRoom);
    },
    settingOpen : function (parent) {
        this.layoutOpen(parent,this.setting);
    },
    unionOpen : function (parent) {
        this.layoutOpen(parent,this.union);
    },
    unionCreateOpen : function (parent) {
        this.layoutOpen(parent,this.unionCreate);
    },
    safeBoxOpen : function (parent) {
        this.layoutOpen(parent,this.safeBox);
    },
    kefuOpen : function (parent) {
        this.layoutOpen(parent,this.kefu);
    },
    statementOpen : function (parent) {
        this.layoutOpen(parent,this.statement);
    },
    holdAdd : function (parent, position, index, pokersSelected, 
                    push1, push2, push3, push4, push5, push6, push7,
                    push8, push9, push10, push11, push12, push13) {
        // cc.isValid(node); 节点是否被销毁
        this.IsValid();
        var prefabHoldNode = cc.instantiate(this.hold);
        prefabHoldNode.id = index;
        // prefabHoldNode.getComponent("Hold").setPoker({"key": "value"});

        // layout.on("touchend", this.onTouchEnded, this); // 添加点击事件
        
        parent.addChild(prefabHoldNode);
        let front = prefabHoldNode.getChildByName("front1");
        front.setPosition(position);
        this._hold = prefabHoldNode;

        prefabHoldNode.getComponent("Hold").setPoker({"key": index});
        console.log("pokersSelected...", pokersSelected);
        this.pokersSelected = pokersSelected;
        this.push1 = push1;
        this.push2 = push2;
        this.push3 = push3;
        this.push4 = push4;
        this.push5 = push5;
        this.push6 = push6;
        this.push7 = push7;
        this.push8 = push8;
        this.push9 = push9;
        this.push10 = push10;
        this.push11 = push11;
        this.push12 = push12;
        this.push13 = push13;

        return prefabHoldNode;
    },

    testPrefab: function () {
        var result = this._hold.getComponent("Hold").getPoker();
        console.log("result...", result);
    },

    getCallPoker: function() {
        return this.callPoker;
    },

    bindPrefabMgrHoldEvent: function (index, selected) {
        var seats = cc.game.seats;
        // console.log("cc.game...", cc.game);
        // console.log("seats...", seats);
        // console.log("seats.pokersSelected...", seats.pokersSelected);

        console.log("this.pokersSelected...", this.pokersSelected);

        var pokersSelected = this.pokersSelected;
        pokersSelected[Number(index)] = selected?1:0;
        let selectedCount = 0;
        for (var i = 0; i < pokersSelected.length; i ++) {
            if (pokersSelected[i] == 1) {
                selectedCount += 1;
            }
        }
        this.push1.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个1";
        this.push2.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个2";
        this.push3.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个3";
        this.push4.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个4";
        this.push5.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个5";
        this.push6.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个6";
        this.push7.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个7";
        this.push8.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个8";
        this.push9.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个9";
        this.push10.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个10";
        this.push11.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个J";
        this.push12.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个Q";
        this.push13.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个K";
        this.callPoker = {
            // 喊了3个1
            poker: 1,
            num: selectedCount,
        }
        
        console.log("index...", index);
        console.log("selected...", selected);
    },

    bindHoldClickEvent : function (node, index, pokersSelected, 
                            push1, push2, push3, push4, push5, push6, push7,
                            push8, push9, push10, push11, push12, push13) {
        // let CCNode = this._hold[index];
        node.on("touchend", (event) => {
            const node = event.target;
            console.log(event);

            // let index = node.id
            console.log("index...", index);
            // index = 5;
            
            let y = 50;
            let front = node;//.getChildByName("front1");
            var v2 = front.getPosition();

            if (pokersSelected[index] == 0) {
                y = 100;
                pokersSelected[index] = 1;
            } else {
                pokersSelected[index] = 0;
            }



            setTimeout(() => {

                front.setPosition(cc.Vec2(v2.x, y));

            }, 0);


            let selectedCount = 0;
            for (var i = 0; i < pokersSelected.length; i ++) {
                if (pokersSelected[i] == 1) {
                    selectedCount += 1;
                }
            }
            push1.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个1";
            push2.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个2";
            push3.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个3";
            push4.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个4";
            push5.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个5";
            push6.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个6";
            push7.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个7";
            push8.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个8";
            push9.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个9";
            push10.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个10";
            push11.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个J";
            push12.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个Q";
            push13.getChildByName("Background").getChildByName("Label").getComponent(cc.Label).string = "我喊" + selectedCount + "个K";

            this.callPoker = {
                // 喊了3个1
                // poker: 1,
                num: selectedCount,
            }

            console.log("callPoker...", this.callPoker);
            

        }, this);
    },

    IsValid : function(){
        if(this._wait && this._wait.isValid == false){
            this._wait = null;  
        }
        if(this._alert && this._alert.isValid == false){
            this._alert = null;  
        }
        if(this._layout && this._layout.isValid == false){
            this._layout = null;  
        }
        if (this._hold && this._hold.isValid == false) {
            this._hold = null;
        }
    },
    waitOpen : function (parent,title,callback) {
        this.IsValid();
        if(this._wait != null){
            return;
        };
        var wait = cc.instantiate(this.wait);
        parent.addChild(wait,7);
        wait.getComponent("Waiting").show(title,callback);
        this._wait = wait;
    },
    alertOpen : function (parent,title,content,onok,needcancel) {
        this.IsValid();
        if(this._alert != null){
            console.log(this._alert);
            return;
        };
        var alert = cc.instantiate(this.alert);
        parent.addChild(alert,6);
        alert.getComponent("Alert").show(title,content,onok,needcancel);
        this._alert = alert;
    },
    layoutOpen : function (parent,prefab) {
        this.IsValid();
        if(this._wait != null || this._alert != null || this._layout != null){
            console.log(this._wait,this._alert,this._layout);
            return;
        };
        var layout = cc.instantiate(prefab);
        parent.addChild(layout,5);
        this._layout = layout;
        this._layout.scale = 0.01;
        this._layout.runAction(cc.scaleTo(0.3, 1).easing(cc.easeBackOut(3.0)));
        /**
         * runAction(action): 节点执行action动作
         * stopAction(action): 停止一个动作
         * stopAllActions(): 停止所有动作
         * 
         * cc.moveTo: 移动节点到某个位置
         * cc.rotateBy: 旋转节点一定到角度
         * cc.scaleTo: 缩放节点
         * 
         * easing: 缓动特效
         */
    },
    layoutOpen2 : function (parent,prefab) {
        this.IsValid();
        if(this._wait != null || this._alert != null || this._layout != null){
            console.log(this._wait,this._alert,this._layout);
            return;
        };
        var layout = cc.instantiate(prefab);
        parent.addChild(layout,5);
        this._layout = layout;
    },
    waitClose : function () {
        if(this._wait){
            try {
                this._wait.removeFromParent(true);
            } catch (error) {
                console.log(error);
            }
            this._wait = null;
        }
    },
    alertClose : function () {
        if(this._alert){
            this._alert.removeFromParent(true);
            this._alert = null;
        }
    },
    layoutClose : function () {
        var self = this;
        cc.vv.audioMgr.playSFX("btn3.mp3");
        var layout = this._layout;
        this._layout = null;
        layout.runAction(cc.sequence(cc.scaleTo(0.3, 0).easing(cc.easeQuarticActionOut(3.0)),cc.callFunc(function(){
            setTimeout(function(){
                layout.removeFromParent();
            },10);
        })));
    },
    layoutClose2 : function () {
        cc.vv.audioMgr.playSFX("btn3.mp3");
        if(this._layout){
            this._layout.removeFromParent();
            this._layout = null;
        }
    }
});
