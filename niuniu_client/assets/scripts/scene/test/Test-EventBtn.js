// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const EventEmitter = require("events");

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.node.on("event", (data) => {
            console.log("btn收到事件,data=", data);
        });
    },

    start () {

    },

    onBtnClick (event, customData) {
        console.log("event...", event, customData);
        this.node.emit("event", "你好");

        // var event = new cc.Event.EventCustom("event", true); // true表示,向上(父节点)传递事件
        // event.data = "hello...";
        // this.node.dispatchEvent(event);
    },

    // update (dt) {},
});
