// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },

        poker: {
            get () {
                return this._poker;
            },
            set (value) {
                this._poker = value;
            },
        },

        _poker: null,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.HOLD_POKER_POSITION_Y = 0;
        this.HOLD_POKER_POSITION_Y_OFFSET = 50;
        
        /**
        // TODO: test case  cocos creator,
        // node组件内, 注册事件poker, 
        // 在组件内emit发送事件poker,这里可以监听到事件和携带的参数
        this.node.on("poker", function(event) {
            console.log("hello poker...", event);
        });
        */
    },

    start () {
        // console.log("hold prefab start...")
    },

    setPoker: function (value) {
        this._poker = value;
        // console.log(this._poker);
        // console.log("hold prefab setPoker...");

        this.isSelected = false;
    },

    getPoker: function () {
        return this._poker;
    },

    onBtnClick: function (event) {
        /**
        this.node.emit("poker", {
            msg: "Hello, this is cocos creator...",
        });
        */

        // console.log("onBtnClick...");
        // this._poker = {"key": 123};
        // console.log("aa...", this._poker, this.id);

        // console.log(event);

        var y = this.HOLD_POKER_POSITION_Y_OFFSET;

        if (this.isSelected) {
            y = this.HOLD_POKER_POSITION_Y;
            this.isSelected = false;
        } else {
            // y = 100;
            this.isSelected = true;
        }

        var parentNode = event.target;
        // var front = parentNode.getChildByName("front1");
        var front = parentNode;
        var v2 = front.getPosition();
        // console.log("v2...", v2);
        front.setPosition(cc.Vec2(v2.x, y));

        cc.vv.prefabMgr.bindPrefabMgrHoldEvent(this._poker.key, this.isSelected);


    },

    // update (dt) {},
});
